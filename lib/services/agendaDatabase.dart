import 'package:my_confinement/models/agenda.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';


class AgendaDatabase {

	static AgendaDatabase _databaseHelper;    // Singleton AgendaDatabase
	static Database _database;                // Singleton Database

	String agendaTable = 'agenda_table';
	String colId = 'id';
	String colTitle = 'title';
	String colDescription = 'description';
	String colDate = 'date';
	String colLieu = 'lieu';
	String colImage = 'image';

	AgendaDatabase._createInstance(); // Named constructor to create instance of AgendaDatabase

	factory AgendaDatabase() {

		if (_databaseHelper == null) {
			_databaseHelper = AgendaDatabase._createInstance(); // This is executed only once, singleton object
		}
		return _databaseHelper;
	}

	Future<Database> get database async {

		if (_database == null) {
			_database = await initializeDatabase();
		}
		return _database;
	}

	Future<Database> initializeDatabase() async {
		// Get the directory path for both Android and iOS to store database.
		Directory directory = await getApplicationDocumentsDirectory();
		String path = directory.path + 'agenda.db';

		// Open/create the database at a given path
		var agendasDatabase = await openDatabase(path, version: 1, onCreate: _createDb);
		return agendasDatabase;
	}

	void _createDb(Database db, int newVersion) async {

		await db.execute('CREATE TABLE $agendaTable($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colTitle TEXT, '
				'$colDescription TEXT, $colDate TEXT, $colLieu TEXT, $colImage TEXT)');
	}

	Future<List<Map<String, dynamic>>> getAgendaMapList() async {
		Database db = await this.database;

//		var result = await db.rawQuery('SELECT * FROM $agendaTable order by $colTitle ASC');
		var result = await db.query(agendaTable, orderBy: '$colTitle ASC');
		return result;
	}

	// Insert Operation: Insert a agenda object to database
	Future<int> insertAgenda(Agenda agenda) async {
		Database db = await this.database;
		var result = await db.insert(agendaTable, agenda.toJson());
		return result;
	}

	// Update Operation: Update a agenda object and save it to database
	Future<int> updateAgenda(Agenda agenda) async {
		var db = await this.database;
		var result = await db.update(agendaTable, agenda.toJson(), where: '$colId = ?', whereArgs: [agenda.id]);
		return result;
	}


	// Delete Operation: Delete a agenda object from database
	Future<int> deleteAgenda(int id) async {
		var db = await this.database;
		int result = await db.rawDelete('DELETE FROM $agendaTable WHERE $colId = $id');
		return result;
	}

	// Get number of agenda objects in database
	Future<int> getCount() async {
		Database db = await this.database;
		List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from $agendaTable');
		int result = Sqflite.firstIntValue(x);
		return result;
	}

	// Get the 'Map List' [ List<Map> ] and convert it to 'agenda List' [ List<Agenda> ]
	Future<List<Agenda>> getAgendaList() async {

		var agendaMapList = await getAgendaMapList(); // Get 'Map List' from database
		int count = agendaMapList.length;         // Count the number of map entries in db table

		List<Agenda> agendaList = List<Agenda>();
		// For loop to create a 'agenda List' from a 'Map List'
		for (int i = 0; i < count; i++) {
			agendaList.add(Agenda.fromJson(agendaMapList[i]));
		}

		return agendaList;
	}
}