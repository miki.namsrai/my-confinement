// To parse this JSON data, do
//
//     final agenda = agendaFromJson(jsonString);

import 'dart:convert';

Agenda agendaFromJson(String str) => Agenda.fromJson(json.decode(str));

String agendaToJson(Agenda data) => json.encode(data.toJson());

class Agenda {
    int id;
    String title;
    String description;
    String date;
    String lieu;
    String image;

    Agenda({
        this.id,
        this.title,
        this.description,
        this.date,
        this.lieu,
        this.image,
    });

    factory Agenda.fromJson(Map<String, dynamic> json) => Agenda(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        date: json["date"],
        lieu: json["lieu"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "date": date,
        "lieu": lieu,
        "image": image,
    };
}
