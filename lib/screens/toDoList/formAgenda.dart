import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:my_confinement/models/agenda.dart';
import 'package:my_confinement/services/agendaDatabase.dart';
import 'package:my_confinement/shared/constants.dart';
import 'package:intl/intl.dart';

class AgendaForm extends StatefulWidget {
  
  final Agenda agenda;
  final String imagePath;

  AgendaForm(this.agenda, this.imagePath);

	@override
  State<StatefulWidget> createState() {
    return AgendaFormState(this.agenda, this.imagePath);
  }
}

class AgendaFormState extends State<AgendaForm> {

  final _formKey = GlobalKey<FormState>();

  AgendaDatabase helper = AgendaDatabase();

  Agenda agenda;

  String _titre;
  String _description;
  String imagePath;
  String _lieu;

  AgendaFormState(this.agenda, this.imagePath);

  @override
  Widget build(BuildContext context) {

    return Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Text(
              'Ajouter une entrée dans l\'agenda',
              style: TextStyle(fontSize: 15.0),
            ),
            SizedBox(
              height: 20.0,
            ),

            //Titre Input
            TextFormField(
              decoration: textBoxTemplate,
              initialValue: "Titre",
              validator: (val) =>
                  val.isEmpty ? 'Veuillez entrer un titre' : null,
              onChanged: (val) => setState(() => _titre = val),
            ),
            SizedBox(
              height: 5.0,
            ),
            //Description Input
            TextFormField(
              decoration: textBoxTemplate,
              initialValue: "Description",
              maxLines: 7,
              validator: (val) =>
                  val.isEmpty ? 'Veuillez entrer une description' : null,
              onChanged: (val) => setState(() => _description = val),
            ),
           
            SizedBox(height: 5.0,),
            //Button confirmation
            RaisedButton(
              color: Colors.black87,
              child: Text(
                'Ajouter',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                print(_titre);
                print(_description);
                print(imagePath);
                setState(() {
                  _save(_titre, _description , DateTime.now(), this.imagePath, 'Lyon');
                });
                
              },
            )
          ],
        ));
  }

	// Save data to database
	void _save(String titre, String description, DateTime date, String image, String lieu) async {
    if (_formKey.currentState.validate()){

      _formKey.currentState.save();

      agenda.title = titre;
      agenda.description = description;
      agenda.image = image;
      agenda.lieu = lieu ;
      if (date == null){
        agenda.date = DateTime.now().toIso8601String();
      }else{
        agenda.date = date.toIso8601String();
      }
      
      print(agenda.date);
      int result;
      if (agenda.id != null) {  // Case 1: Update operation
        result = await helper.updateAgenda(agenda);
      } else { // Case 2: Insert Operation
        result = await helper.insertAgenda(agenda);
      }

      if (result != 0) {  // Success
        //_showAlertDialog('Status', 'Agenda Saved Successfully');
      } else {  // Failure
        //_showAlertDialog('Status', 'Problem Saving Agenda');
      }
      Navigator.pop(context);
      Navigator.pop(context);
    }
	}
}
