import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:my_confinement/shared/constants.dart';
import 'package:intl/intl.dart';
import 'package:my_confinement/models/toDo.dart';
import 'package:my_confinement/services/databaseHelper.dart';
import 'package:my_confinement/widgets/notification/notificationManager.dart';

class FormAddItem extends StatefulWidget {
  
  final Todo todo;
  final NotificationManager manager;

  FormAddItem(this.todo, this.manager);

	@override
  State<StatefulWidget> createState() {

    return FormAddItemState(this.todo);
  }
}

class FormAddItemState extends State<FormAddItem> {

  final _formKey = GlobalKey<FormState>();
  final List<String> categorie = ['Alimentation', 'Santé', 'Sport', 'Animaux','Domestique'];

  DatabaseHelper helper = DatabaseHelper();

  Todo todo;

  String _titre;
  DateTime _date;
  String _categorie;

  FormAddItemState(this.todo);

  @override
  Widget build(BuildContext context) {

    return Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Text(
              'Ajouter une tâche à accomplir',
              style: TextStyle(fontSize: 15.0),
            ),
            SizedBox(
              height: 20.0,
            ),

            //Titre Input
            TextFormField(
              decoration: textBoxTemplate,
              validator: (val) =>
                  val.isEmpty ? 'Veuillez entrer un titre' : null,
              onChanged: (val) => setState(() => _titre = val),
            ),
            //Dropdown Catégorie
            DropdownButtonFormField(
              validator: (value){
                if(value.isEmpty){
                  return 'Sélectionner une catégorie';
                }
              },
              hint: Text("Sélectionner une catégorie", style: TextStyle(fontSize: 14),),
              value: _categorie,
              items: categorie.map((ctg) {
                return DropdownMenuItem(
                  value: ctg,
                  child: Text(ctg),
                );
              }).toList(),
              onChanged: (value) => setState(() => _categorie = value),
            ),
            //Date pick
            FlatButton(
                onPressed: () {
                  DatePicker.showDateTimePicker(context,
                    showTitleActions: true,
                    minTime: DateTime.now(), 
                    onChanged: (date) {
                      print('change $date');
                    }, 
                    onConfirm: (date) {
                      setState(() => _date = date);
                    },
                    currentTime: DateTime.now(), 
                    locale: LocaleType.fr);
                },
                child: Text(
                  'Choisir une deadline',
                  style: TextStyle(color: Colors.red[900]),
                )
              ),
            Text(
              formatDate(_date ?? DateTime.now(), [dd, '/', mm, '/', yyyy]).toString(),
              style: TextStyle(fontSize: 18.0),
            ),
            SizedBox(height: 5.0,),
            Text(
              formatDate(_date ?? DateTime.now(), [HH, ':', nn]).toString(),
              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w200, color: Colors.grey),
            ),
            SizedBox(height: 5.0,),
            //Button confirmation
            RaisedButton(
              color: Colors.black87,
              child: Text(
                'Ajouter',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                print(_titre);
                print(_categorie);
                print(_date);

                setState(() {
                  _save(_titre, _categorie, _date, widget.manager);
                });
                
              },
            )
          ],
        ));
  }

	// Save data to database
	void _save(String titre, String categorie, DateTime deadline, NotificationManager manager) async {
    if (_formKey.currentState.validate()){

      _formKey.currentState.save();

      todo.title = titre;
      todo.categorie = categorie;
      if (deadline == null){
        todo.deadline = DateTime.now().toIso8601String();
      }else{
        todo.deadline = deadline.toIso8601String();
      }
      
      print(todo.deadline);
      int result;
      if (todo.id != null) {  // Case 1: Update operation
        result = await helper.updateTodo(todo);
      } else { // Case 2: Insert Operation
        result = await helper.insertTodo(todo);
      }

      if (result != 0) {  // Success
        manager.showNotification(todo.title, deadline);
        //_showAlertDialog('Status', 'Todo Saved Successfully');
      } else {  // Failure
        //_showAlertDialog('Status', 'Problem Saving Todo');
      }
      Navigator.pop(context);
    }
	}
}
