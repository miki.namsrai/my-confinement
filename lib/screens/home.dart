import 'package:flutter/material.dart';
import 'package:my_confinement/widgets/sidebar/navbloc.dart';

class Home extends StatelessWidget with NavigationStates {
  
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "Accueil",
        style: TextStyle(fontWeight: FontWeight.w900, fontSize: 28),
      ),
    );
  }
}