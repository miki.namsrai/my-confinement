import 'package:flutter/material.dart';
import 'package:my_confinement/widgets/agenda/agenda.dart';
import 'package:my_confinement/widgets/sidebar/navbloc.dart';

class Agenda extends StatelessWidget with NavigationStates{
  @override
  Widget build(BuildContext context) {
    return Center(
      child: AgendaContainer()
    );
  }
}