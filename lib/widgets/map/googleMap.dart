import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/services.dart' show rootBundle;

import 'package:http/http.dart' as http;
import 'package:my_confinement/models/velov.dart';

class GMap extends StatefulWidget {
  @override
  _GMapState createState() => _GMapState();
}

class _GMapState extends State<GMap> {

  GoogleMapController _mapController;
  String _mapStyle;

  BitmapDescriptor pinLocationIcon;
  BitmapDescriptor pinVelov;
  Set<Marker> _markers = {};

  LatLng _center = const LatLng(40.688841, -74.044015); //New York
  double _zoom = 11.0;

  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;
    _mapController.setMapStyle(_mapStyle);

    _getLocation();
    _placeVelov();
  }

  @override
  void initState() {
    super.initState();
    rootBundle.loadString('assets/styles/mapStyle.txt').then((string) {
      _mapStyle = string;
    });
    setCustomMapPin();
  }

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(devicePixelRatio: 2.5),
      'assets/img/pinMap.png');
    pinVelov = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(devicePixelRatio: 2.5),
      'assets/img/velo-v.png');
   }

  Future<Velov>_getVelov() async {
    //String url = 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=station-velov-grand-lyon&facet=name&facet=commune&facet=bonus&facet=status&facet=available&facet=availabl_1&facet=availabili&facet=availabi_1&facet=last_upd_1';
    String url = 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=station-velov-grand-lyon&rows=348&facet=name&facet=commune&facet=status&facet=available';
    //var url = 'https://rickandmortyapi.com/api/character/1';
    final http.Response response = await http.get(url);

    return velovFromJson(response.body);
    //print(json);
  }

  _placeVelov() async {
    Velov velov = await _getVelov();

    for (var vField in velov.records){
      print(vField.fields.geoShape.coordinates);
      setState(() {
        _markers.add(
          Marker(
            markerId: MarkerId(vField.recordid),
            position: LatLng(vField.fields.geoPoint2D.first,vField.fields.geoPoint2D.last),
            icon: pinVelov,
            infoWindow: InfoWindow(title: vField.fields.name),
          )
        );
      });
    }
  }

  void _getLocation() async {
    var currentLocation = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    print(currentLocation.latitude.toString());
    print(currentLocation.longitude.toString());
    
    //_mapController.moveCamera(CameraUpdate.newLatLngZoom(LatLng(currentLocation.latitude,currentLocation.longitude), 15));
    _mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(currentLocation.latitude,currentLocation.longitude), zoom: 15)));

    setState(() {
        _markers.add(
          Marker(
              markerId: MarkerId('my_location'),
              position: LatLng(currentLocation.latitude,currentLocation.longitude),
              icon: pinLocationIcon,
              infoWindow: InfoWindow(title: 'Votre position'),
          )
        );
    });
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
      child: GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: _zoom,
          ),
          markers: _markers,
        ), 
    );
  }
}