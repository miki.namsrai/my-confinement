import 'dart:io';

import 'package:flutter/material.dart';
import 'package:my_confinement/models/agenda.dart';
import 'package:my_confinement/services/agendaDatabase.dart';
import 'package:my_confinement/widgets/agenda/camera.dart';
import 'package:my_confinement/widgets/todoList/swipeWidget.dart';
import 'package:sqflite/sqflite.dart';

class AgendaContainer extends StatefulWidget {
  @override
  _AgendaContainerState createState() => _AgendaContainerState();
}

class _AgendaContainerState extends State<AgendaContainer> {

  AgendaDatabase agendaDatabase = AgendaDatabase();
  List<Agenda> agendaList;

  int count = 0;

  bool refresh = false;


  @override
  Widget build(BuildContext context) {

    if (agendaList == null) {
      agendaList = List<Agenda>();
      updateListView();
    }

    return Container(
      height: MediaQuery.of(context).size.height,
      child: Scaffold(body: _myLV(context),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        child: Icon(
          Icons.add,color: Colors.white,
        ),
        onPressed: (){
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CameraWidget(),
            ),
          );
        },
      ),)
    );
  }

  Widget _myLV(BuildContext context){
    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int position) {
          return new OnSlide(
            items: <ActionItems>[/*
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(Icons.mode_edit),
                    onPressed: () {},
                    color: Colors.blue,
                  ),
                  onPress: () {
                  },
                  backgroudColor: Colors.white),*/
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(Icons.delete),
                    onPressed: () {
                    },
                    color: Colors.red,
                  ),
                  onPress: () {
                    _delete(context, agendaList[position]);
                  },
                  backgroudColor: Colors.white),
            ],
            child: Container(
              padding: const EdgeInsets.only(top:10.0),
              width: 200.0,
              height: 100.0,  
                child: new Card(
                color: Colors.white,
                elevation: 2.0,
                child: ListTile(
                  leading: Image.file(File(agendaList[position].image)),
                  title: Text(this.agendaList[position].title,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  subtitle: Text(this.agendaList[position].description),
                  /*trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      GestureDetector(
                        child: Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        onTap: () {
                          _delete(context, todoList[position]);
                        },
                      ),
                    ],
                  ),
                  */
                  onTap: () {
                    debugPrint("ListTile Tapped");
                  },
                ),
              ),
            )
          );
        },
    );
  }

  void _delete(BuildContext context, Agenda agenda) async {
    int result = await agendaDatabase.deleteAgenda(agenda.id);
    if (result != 0) {
      _showSnackBar(context, 'Item Deleted Successfully');
      updateListView();
    }
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void updateListView() {
    final Future<Database> dbFuture = agendaDatabase.initializeDatabase();
    dbFuture.then((database) {
      Future<List<Agenda>> agendaListFuture = agendaDatabase.getAgendaList();
      agendaListFuture.then((agendaList) {
        setState(() {
          this.agendaList = agendaList;
          this.count = agendaList.length;
        });
      });
    });
  }
  
}
