import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:my_confinement/models/agenda.dart';
import 'package:my_confinement/screens/toDoList/formAgenda.dart';
import 'package:my_confinement/widgets/agenda/agenda.dart';
import 'package:path_provider/path_provider.dart';

List<CameraDescription> cameras = [];

class CameraWidget extends StatefulWidget {
  @override
  _CameraWidgetState createState() => _CameraWidgetState();
}

class _CameraWidgetState extends State<CameraWidget> {

  CameraController _cameraController;
  Future<void> _initializeControllerFuture;

  bool isCameraReady = false;
  bool showCapturePhoto = false;
  var imagePath;

  int selectedCameraIdx;
  bool isSwitched = false;

  @override
  void initState(){
    super.initState();
    _fetchCameras();
  }

  Future<void> _fetchCameras() async{
    availableCameras().then((availableCameras){
      cameras = availableCameras;

      if (cameras.length > 0){
        setState(() {
          selectedCameraIdx = 0;
        });
        _initializeCamera(cameras[selectedCameraIdx]);
      }else{
        print("No camera");
      }
    });

  }

  Future<void> _initializeCamera(CameraDescription cameraDescription) async {

    if (_cameraController != null) {
      await _cameraController.dispose();
    }

    _cameraController = CameraController(cameraDescription, ResolutionPreset.high);
    _initializeControllerFuture = _cameraController.initialize();

    if(!mounted){
      return;
    }else{
      setState(() {
        isCameraReady = true;
      });
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _cameraController != null
          ? _initializeControllerFuture = _cameraController.initialize()
          : null; //on pause camera is disposed, so we need to call again "issue is only for android"
    }
  }

  void onCaptureButtonPressed() async {
    try{
      final path= '${(await getTemporaryDirectory()).path}/my_confinement_${DateTime.now()}.png';

      await _cameraController.takePicture(path); //take photo

      setState(() {
        showCapturePhoto = true;
        imagePath = path;
      });

      // If the picture was taken, display it on a new screen.
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DisplayPictureScreen(imagePath: path),
        ),
      );

    }catch(e){
      print(e);
    }
  }

  Image images(){
    if(imagePath == null){
      print('no');
      return null;
    }else{
      return Image.file(File(imagePath));
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _cameraController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final deviceRatio = size.width / size.height;

    return Scaffold(
      appBar: AppBar(title: Text('Photo')),
      body: Column(
        children: <Widget>[
          Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height*0.8,
            child: FutureBuilder<void>(
                future: _initializeControllerFuture,
                builder: (context, snapshot){
                  if(snapshot.connectionState == ConnectionState.done){
                    return CameraPreview(_cameraController);
                  }else{
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
            ),
          ),
          _captureControlRowWidget(),
          
        ],
      ),
    );
  }

  /// Returns a suitable camera icon for [direction].
  IconData _getCameraLensIcon(CameraLensDirection direction) {
    switch (direction) {
      case CameraLensDirection.back:
        return Icons.camera_rear;
      case CameraLensDirection.front:
        return Icons.camera_front;
      case CameraLensDirection.external:
        return Icons.camera;
    }
    throw ArgumentError('Unknown lens direction');
  }

  Widget _captureControlRowWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(height: 5.0,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(width: MediaQuery.of(context).size.width*0.225,),
            FloatingActionButton(
              backgroundColor: Colors.white,
              child: Icon(Icons.camera_alt,
              color: Colors.black,
              
              ),
              onPressed: _cameraController != null &&
                  _cameraController.value.isInitialized &&
                  !_cameraController.value.isRecordingVideo
                  ? onCaptureButtonPressed
                  : null,
            ),
            _cameraTogglesRowWidget()
          ],
        ),
        
        
      ],
    );
  }

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _cameraTogglesRowWidget() {
    if (cameras == null || cameras.isEmpty) {
    return Spacer();
  }

  CameraDescription selectedCamera = cameras[selectedCameraIdx];
  CameraLensDirection lensDirection = selectedCamera.lensDirection;

  return FlatButton.icon(
          onPressed: _onSwitchCamera,
          icon: Icon(_getCameraLensIcon(lensDirection),color: Colors.black,),
          label: Text(''),
  );

  }
  void _onSwitchCamera() {
  selectedCameraIdx =
  selectedCameraIdx < cameras.length - 1 ? selectedCameraIdx + 1 : 0;
  CameraDescription selectedCamera = cameras[selectedCameraIdx];
  _initializeCamera(selectedCamera);
}

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (_cameraController != null) {
      await _cameraController.dispose();
    }
    _cameraController = CameraController(
      cameraDescription,
      ResolutionPreset.high,
    );

    // If the controller is updated then update the UI.
    _cameraController.addListener(() {
      if (mounted) setState(() {});
      if (_cameraController.value.hasError) {
        print('camera error');
      }
    });

    try {
      await _cameraController.initialize();
    } on CameraException catch (e) {
      //_showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }
}



class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Display the Picture'),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.save),
          color: Colors.black,
          onPressed: ()async {
            await showModalBottomSheet(
                context: context,
                builder: (context) {
                  return Container(
                    padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
                    child: AgendaForm(Agenda(),imagePath),
                  );
                }).whenComplete(() {
               Navigator.pop(context);
              
            });
          },
        )
      ],),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Image.file(File(this.imagePath)),
    );
  }
}
