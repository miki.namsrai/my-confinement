import 'package:bloc/bloc.dart';
import 'package:my_confinement/screens/home.dart';
import 'package:my_confinement/screens/toDoList/toDoList.dart';
import 'package:my_confinement/screens/map.dart';
import 'package:my_confinement/screens/compass.dart';
import 'package:my_confinement/screens/agenda.dart';


enum NavigationEvents {
  HomeClickedEvent,
  ToDoListClickedEvent,
  MapClickedEvent,
  CompassClickedEvent,
  AgendaClickedEvent
}

abstract class NavigationStates {}

class NavigationBloc extends Bloc<NavigationEvents, NavigationStates> {
  @override
  NavigationStates get initialState => Home();

  @override
  Stream<NavigationStates> mapEventToState(NavigationEvents event) async* {
    switch (event) {
      case NavigationEvents.HomeClickedEvent:
        yield Home();
        break;
      case NavigationEvents.ToDoListClickedEvent:
        yield ToDoList();
        break;
      case NavigationEvents.MapClickedEvent:
        yield GoogleMap();
        break;
      case NavigationEvents.CompassClickedEvent:
        yield Compass();
        break;  
      case NavigationEvents.AgendaClickedEvent:
        yield Agenda();
        break;
    }
  }
}