import 'package:flutter/material.dart';
import 'package:flutter_compass/flutter_compass.dart';
import 'package:my_confinement/widgets/compass/compassPainter.dart';

class CompassWidget extends StatefulWidget {

  CompassWidget({Key key}) : super(key: key);

  @override
  _CompassWidgetState createState() => _CompassWidgetState();
}

class _CompassWidgetState extends State<CompassWidget> {

  double _heading = 0;

  String get _readout => _heading.toStringAsFixed(0) + '°';

  @override
  void initState() {
      
      super.initState();
      FlutterCompass.events.listen(_onData);
  }
  
  void _onData(double x) => setState(() { _heading = x; });

  final TextStyle _style = TextStyle(
      color: Colors.red, 
      fontSize: 32, 
      fontWeight: FontWeight.w200,
  );
    
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        foregroundPainter: CompassPainter(angle: _heading),
        child: Center(child: Text(_readout, style: _style))
    );
  }
}